# Arezzo 26 marzo 2021
# Analysing Available Flight Space
rm(list = ls())

library(tidyverse)
load(file = "Rdata/dfmain1.Rdata")
# IMPORT TLS (PROCESSED) DATA
dfmain1 <- dfmain1 %>% mutate(stratum = 
                                factor(stratum, 
                                       levels = c("FF", "Sh", "LU", "UU", "Sc")))
dfmain1 %>% head()

# IMPORT FIELD DATA (TRADITIONAL)
cav <- read.csv('csv/database_rilievi_Sila_2019agosto - piedilista.csv')
cav <- cav %>% mutate(gi_mq = pi*(diam_cm/200)^2)
cav$FT <- substr(cav$ID_DEF, 4,4)

ExpFact <- 10000/(pi*15^2)
SummaryForTab1 <- cav %>% group_by(ID_DEF, FT) %>% 
  summarise(NStem = length(gi_mq),
            QMD = sqrt(sum(diam_cm^2)/length(gi_mq)),
            BAplot=sum(gi_mq)) %>% 
  mutate(StemDens.ha = NStem*ExpFact,
         BA.ha = BAplot*ExpFact) %>% 
  rename(iddef = ID_DEF, FTita = FT)

tab1 <- cav %>% group_by(ID_DEF) %>% count() %>% 
  left_join(
    cav %>% dplyr::filter(diam_cm > 0 & diam_cm <= 13) %>% group_by(ID_DEF) %>% 
      summarise(small.trees = length(diam_cm),
                ba.small.trees = sum(gi_mq))) %>% 
  left_join(
    cav %>% dplyr::filter(diam_cm > 9 & diam_cm <= 28) %>% group_by(ID_DEF) %>% 
      summarise(medium.trees = length(diam_cm),
                ba.medium.trees = sum(gi_mq))) %>% 
  rename(iddef = ID_DEF)


df.c0 <- dfmain1 %>% 
  dplyr::select(-c('Xclass2', 'xmin', 'xmax',
                   'Yclass2', 'ymin', 'ymax',
                   'Zclass2', 'zmin', 'zmax')) %>% 
  mutate(vegVox = case_when(
    emptyVox == 0 ~ 1,
    emptyVox == 1 ~ 0
  )) %>% 
  group_by(idplot, FTita, FT, stratum) %>% 
  summarise(PDI = mean(vegVox)) %>% 
  rename(iddef = idplot) %>% 
  left_join(SummaryForTab1) %>% 
  dplyr::filter(stratum != "Sc") %>% 
  mutate(idplot = substr(iddef,4,7)) %>% 
  rename(id_def = iddef) %>% 
  pivot_wider(names_from = "stratum", values_from = "PDI")
df.c0 %>% head()


# ADD CLARK EVANS values
dfout <- read.csv("csv/df_24plots.csv") %>% 
  dplyr::select('id_def', 'NtreeTLS', 'nTreesALS',
                'SCI', 'ESCI',
                'CE_naive', 'CE_cdf')
dfout %>% head


dfc <- df.c0 %>% left_join(dfout) # versione wide con tutte le variabili di interesse

dfc %>% head()



