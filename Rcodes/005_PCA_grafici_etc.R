# Arezzo 30 marzo 2021
rm(list = ls())

library(tidyverse)
library(ggrepel)
library(ggforce)

circleFun <- function(center = c(0,0),diameter = 1, npoints = 100){
  r = diameter / 2
  tt <- seq(0,2*pi,length.out = npoints)
  xx <- center[1] + r * cos(tt)
  yy <- center[2] + r * sin(tt)
  return(data.frame(x = xx, y = yy))
}


inData <- "dfc_2.csv" %>% read.csv
inData %>% head()

PCAmain <- inData %>% mutate(
  Clusters = factor(Cluster.3_4var),
  iddef = substr(id_def, 4, 7)) %>% 
  ggplot() +
  geom_point(aes(Prin1, Prin2, colour = Clusters), size = 3) + 
  xlim(-4, 4) + ylim(-4, 4) +
  xlab('Component 1 (60.4%)') + ylab('Component 2 (30.2%)') +  
  geom_hline(yintercept=0) + geom_vline(xintercept=0) +
  geom_text_repel(aes(Prin1, Prin2, label = iddef), size = 4) +
  coord_fixed() +
  geom_mark_ellipse(aes(Prin1, Prin2, fill = Clusters)) +
  theme(plot.margin = unit(c(1, 3, .5, .5), "cm")) # top, right, bottom, and left 
PCAmain

magic.circle <- data.frame(stratum = c('FF', 'Sh', 'LU', 'UU'),
           Prin1 = c(-0.02787, 0.87987, 0.95095, 0.85756),
           Prin2 = c(0.97478, 0.32480, 0.07217, -0.38160))

dat <- circleFun(c(0,0),2,npoints = 100)

inset <- ggplot() +
  # geom_point(data = magic.circle, aes(x=Prin1, y = Prin2, color = stratum), size = 5) +
  geom_segment(
    data = magic.circle,
    aes(x = 0, y = 0, xend = Prin1, yend = Prin2),
    lineend = "round", linejoin = "bevel",
    size = 1, 
    arrow = arrow(length = unit(0.1, "inches"))) +
  xlim(-1,1) + ylim(-1,1) +
  geom_hline(yintercept=0, lty = 2) + geom_vline(xintercept=0, lty = 2) +
  xlab('Component 1 (60.4%)') + ylab('Component 2 (30.2%)') + 
  theme(legend.position = "none",
        axis.title=element_text(size=8)) + coord_fixed() +
  geom_text(data = magic.circle %>% dplyr::filter(stratum=='FF'),
            aes(Prin1, Prin2, label = stratum),
            nudge_x = -.3, nudge_y = -.2) +
  geom_text(data = magic.circle %>% dplyr::filter(stratum=='Sh'),
            aes(Prin1, Prin2, label = stratum),
            nudge_x = -.05, nudge_y = .2) +
  geom_text(data = magic.circle %>% dplyr::filter(stratum=='LU'),
            aes(Prin1, Prin2, label = stratum),
            nudge_x = -.05, nudge_y = -.2) +
  geom_text(data = magic.circle %>% dplyr::filter(stratum=='UU'),
            aes(Prin1, Prin2, label = stratum),
            nudge_x = -.3, nudge_y = -.2) +
  geom_path(data = dat,aes(x,y))
inset


PCAfig.def <- PCAmain + 
  annotation_custom(ggplotGrob(inset),
                    xmin = 2.2, xmax = 6.0, 
                    ymin = 1.4, ymax = 5.2)


ggsave(filename = "figures/PCA_figure.png", plot = PCAfig.def, width = 10, height = 10)
